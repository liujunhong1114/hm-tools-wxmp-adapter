package top.hmtools.wxmp.core.access_handle;

import top.hmtools.wxmp.core.configuration.AppIdSecretBox;
import top.hmtools.wxmp.core.model.AccessTokenBean;

/**
 * 基础的access token中间件
 * @author HyboWork
 *
 */
public abstract class BaseAccessTokenHandle {

	private AppIdSecretBox _appIdSecretBox;
	
	public BaseAccessTokenHandle(AppIdSecretBox appIdSecretBoxParam) {
		if(appIdSecretBoxParam == null){
			throw new RuntimeException("AppIdSecretBox 不能为空");
		}
		this._appIdSecretBox = appIdSecretBoxParam;
	}
	
	/**
	 * 获取访问微信公众号服务api接口用的access token 字符串
	 * @return
	 */
	public String getAccessTokenString(){
		AccessTokenBean accessTokenBean = this.getAccessTokenBean();
		if(accessTokenBean == null){
			return null;
		}
		return accessTokenBean.getAccess_token();
	}
	
	/**
	 * 获取访问微信公众号服务api接口用的access token 对象实例
	 * @return
	 */
	public abstract AccessTokenBean getAccessTokenBean();

	/**
	 * 获取 存储 appid、appsecret 数据对的盒子
	 * @return
	 */
	public AppIdSecretBox getAppIdSecretBox() {
		return _appIdSecretBox;
	}
	
	
	
}

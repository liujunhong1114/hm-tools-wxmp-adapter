package top.hmtools.wxmp.material.model;

import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class BatchgetMaterialResult extends ErrcodeBean {

	private int total_count;
	
	private int item_count;
	
	private List<ItemResult> item;

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public int getItem_count() {
		return item_count;
	}

	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}

	public List<ItemResult> getItem() {
		return item;
	}

	public void setItem(List<ItemResult> item) {
		this.item = item;
	}

	@Override
	public String toString() {
		return "BatchgetMaterialResult [total_count=" + total_count + ", item_count=" + item_count + ", item=" + item
				+ "]";
	}
	
	
}

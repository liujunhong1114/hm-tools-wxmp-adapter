package top.hmtools.wxmp.material.model;

import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class MaterialResult extends ErrcodeBean {

	/**
	 * 视频标题
	 */
	private String title;
	
	/**
	 * 视频说明
	 */
	private String description;
	
	/**
	 * 视频URL
	 */
	private String down_url;
	
	/**
	 * 图文素材集合
	 */
	private List<Articles> news_item;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDown_url() {
		return down_url;
	}

	public void setDown_url(String down_url) {
		this.down_url = down_url;
	}

	public List<Articles> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<Articles> news_item) {
		this.news_item = news_item;
	}

	@Override
	public String toString() {
		return "MaterialResult [title=" + title + ", description=" + description + ", down_url=" + down_url
				+ ", news_item=" + news_item + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}

package top.hmtools.wxmp.menu.models.simple;

import java.util.List;

public class SelfmenuInfoSubButtonBean {

	private List<SelfmenuInfoButtonBean> list;

	public List<SelfmenuInfoButtonBean> getList() {
		return list;
	}

	public void setList(List<SelfmenuInfoButtonBean> list) {
		this.list = list;
	}
	
	
}
